build:
	docker-compose build --no-cache --force-rm
stop:
	docker-compose stop
up:
	docker-compose up -d
install:
	docker exec -it next-rothumb sh -c "npm install"
serv:
	docker exec -it next-rothumb sh
run:
	docker exec -it next-rothumb sh -c "npm run dev"