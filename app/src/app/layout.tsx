import type { Metadata } from "next";
import ReduxProvider from "@/provider/redux/ReduxProvider";

import "@/assets/css/app.css";
import NavMenu from "@/components/common/NavMenu";

export const metadata: Metadata = {
    title: "Rule of Thumb",
    description: "Rule of Thumb description.",
};

export default function RootLayout({
    children,
}: {
    children: React.ReactNode;
}) {
    return (
        <ReduxProvider>
            <html lang="en">
                <body>
                    <NavMenu />
                    {children}
                </body>
            </html>
        </ReduxProvider>
    );
}
