import BottomBanner from "@/components/common/BottomBanner";
import Footer from "@/components/common/Footer";
import Header from "@/components/common/Header";
import People from "@/components/common/People";
import TopBanner from "@/components/common/TopBanner";

export default async function Home() {
    return (
        <div>
            <Header />
            <div className="max-centered">
                <TopBanner />
                <People />
                <BottomBanner />
                <hr role="separator"></hr>
                <Footer />
            </div>
        </div>
    );
}
