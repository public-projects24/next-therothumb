import "@/assets/css/components/common/dropdown.css";
import { changeViewType } from "@/store/peopleSlice";
import { useAppDispatch, useAppSelector } from "@/hooks/reduxHooks";
import { useState } from "react";

export default function DropDown() {
    const dispatch = useAppDispatch();
    const people = useAppSelector((state) => state.people);
    const [dropDownMenuIsOpen, setDropDownMenuIsOpen] = useState(false);

    const handleClickDropDowItem = (
        e: React.SyntheticEvent,
        viewType: ViewType
    ) => {
        e.preventDefault();

        dispatch(changeViewType(viewType));
        setDropDownMenuIsOpen(false);
    };

    return (
        <ul className="dropdown">
            <li className="dropdown__list">
                <a
                    href="#"
                    className="dropdown__link"
                    onClick={(e) => {
                        e.preventDefault();
                        setDropDownMenuIsOpen(!dropDownMenuIsOpen);
                    }}
                >
                    <span className="dropdown__link-label">
                        {people.viewType}
                    </span>
                    <img src="img/people/triangle.svg" alt="" />
                </a>
                <ul
                    className={`dropdown__menu ${
                        dropDownMenuIsOpen ? "dropdown__menu--active" : ""
                    }`}
                >
                    <li
                        className="dropdown__menu-item"
                        onClick={(e) => handleClickDropDowItem(e, "list")}
                    >
                        List
                    </li>
                    <li
                        className="dropdown__menu-item"
                        onClick={(e) => handleClickDropDowItem(e, "grid")}
                    >
                        Grid
                    </li>
                </ul>
            </li>
        </ul>
    );
}
