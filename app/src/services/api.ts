import axios from "axios";

const api = axios.create({
    baseURL: process.env.NEXT_PUBLIC_API_ENDPOINT,
    //baseURL: "/api/",
    headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
    },
});

api.interceptors.response.use(
    function (response) {
        // Any status code that lie within the range of 2xx cause this function to trigger
        // Do something with response data
        return response;
    },
    function (error) {
        // Any status codes that falls outside the range of 2xx cause this function to trigger
        // Do something with response error
        interceptorOnError(error);
        return Promise.reject(error);
    }
);

async function interceptorOnError(error: any) {
    console.log("error interceptor ", error.response);
}
export default api;
