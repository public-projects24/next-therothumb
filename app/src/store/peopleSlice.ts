import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import Cookies from "js-cookie";

interface PeopleState {
    viewType: ViewType;
    persons: Person[];
}

const initialState: PeopleState = {
    viewType: "grid",
    persons: [],
};

export const peopleSlice = createSlice({
    name: "people",

    initialState,
    reducers: {
        incrementVote: (
            state,
            action: PayloadAction<{
                personName: string;
                voteType: "positive" | "negative";
            }>
        ) => {
            const person = state.persons.find(
                (person) => person.name === action.payload.personName
            );

            if (person) {
                action.payload.voteType === "positive"
                    ? person.votes.positive++
                    : person.votes.negative++;

                peopleSlice.caseReducers.savePersons(state);
            }
        },
        changeViewType: (state, action: PayloadAction<ViewType>) => {
            state.viewType = action.payload;
        },
        initPersons: (state, action: PayloadAction<Person[]>) => {
            state.persons = action.payload;
        },
        savePersons: (state) => {
            Cookies.set("persons", JSON.stringify(state.persons));
        },
    },
});

export const { incrementVote, changeViewType, initPersons } =
    peopleSlice.actions;

export default peopleSlice.reducer;
