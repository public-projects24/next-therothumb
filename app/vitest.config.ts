import { defineConfig } from "vitest/config";
import react from "@vitejs/plugin-react";
import path from "path";

export default defineConfig({
    plugins: [react()],
    test: {
        environment: "jsdom",
    },
    resolve: {
        alias: {
            "@": path.resolve(__dirname, "./src"), // Cuando solo se usa Vue
            //"@": path.resolve(__dirname, "./"), //Cuando se usa Nuxt
        },
    },
});
